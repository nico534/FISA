package de.fraunhofer.iosb.ilt.fisabackend.model;

public class MapNameListElement {
    private String name;
    private Long id;

    /**
     * New ListElement with Name and ID
     * @param name
     * @param id
     */
    public MapNameListElement(String name, Long id) {
        this.name = name;
        this.id = id;
    }

    /**
     * @return The name of the ListElement
     */
    public String getName() {
        return name;
    }

    /**
     * @return The ID of the ListElement
     */
    public Long getId() {
        return id;
    }
}
