import { FisaId } from './valueTypes';

export interface AvailableFisaDocumentI {
  name: string;
  id: FisaId;
}

export interface AvailableProjectI {
  name: string;
  id: FisaId;
}
