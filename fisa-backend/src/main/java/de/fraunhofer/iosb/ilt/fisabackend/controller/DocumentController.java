package de.fraunhofer.iosb.ilt.fisabackend.controller;

import de.fraunhofer.iosb.ilt.fisabackend.model.MapNameListElement;
import de.fraunhofer.iosb.ilt.fisabackend.model.definitions.FisaDocument;
import de.fraunhofer.iosb.ilt.fisabackend.service.DocumentService;
import de.fraunhofer.iosb.ilt.fisabackend.service.exception.ClientRequestException;
import de.fraunhofer.iosb.ilt.fisabackend.service.repository.FisaDocumentRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

/**
 * The controller managing frontend requests related to FisaDocuments.
 */
@RestController
@RequestMapping("/documents")
public class DocumentController {
    private DocumentService documentService;
    private static final Logger LOGGER = LoggerFactory.getLogger(DocumentController.class);
    /**
     * Instantiates a new Document controller.
     *
     * @param repo The Document-Repository of FISA
     */
    public DocumentController(
            FisaDocumentRepository repo) {
        this.documentService = new DocumentService(repo);
    }

    /**
     * Endpoint for listing all available FisaDocuments on the server.
     *
     * @return All available FisaDocuments as a tuple of ID and Name
     */
    @CrossOrigin(origins = "*")
    @GetMapping("/")
    public ResponseEntity listFisaDocuments() {

        try {
            List<MapNameListElement> documents = documentService.listFisaDocument();
            return ResponseEntity.ok(documents);
        } catch (IOException e) {
            LOGGER.error("Failed to fulfill request", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        }
    }

    /**
     * Endpoint for creating a FisaDocument
     *
     * @return Response of backend operation
     * @param document The FisaDocument that should be created
     */
    @CrossOrigin(origins = "*")
    @PostMapping("/")
    public ResponseEntity createFisaDocument(@RequestBody FisaDocument document) {
        try {
            documentService.createFisaDocument(document);
            return ResponseEntity.ok("stored successfully");
        } catch (IOException e) {
            LOGGER.error("Failed to fulfill request", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        } catch (ClientRequestException e) {
            LOGGER.error("Failed to fulfill request", e);
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    /**
     * Endpoint for fetching one specific FisaDocument
     *
     * @param id The ID of the requested FisaDocument
     *
     * @return The FisaDocument
     */
    @CrossOrigin(origins = "*")
    @GetMapping("/{uuid}")
    public ResponseEntity getFisaDocument(@PathVariable(value = "uuid")Long id) {
        FisaDocument document;
        try {
            document = documentService.getFisaDocument(id);
            return ResponseEntity.ok(document);
        } catch (IOException e) {
            LOGGER.error("Failed to fulfill request", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        } catch (ClientRequestException e) {
            LOGGER.error("Failed to fulfill request", e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    /**
     * Endpoint for deleting the FisaDocument
     *
     * @return Response of backend operation
     * @param id The ID of the FisaDocument to delete
     */
    @CrossOrigin(origins = "*")
    @DeleteMapping("/{uuid}")
    public ResponseEntity deleteFisaDocument(@PathVariable(value = "uuid")Long id) {
        try {
            documentService.deleteFisaDocument(id);
            return ResponseEntity.ok("deleted successfully");
        } catch (IOException e) {
            LOGGER.error("Failed to fulfill request", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(e.getMessage());
        } catch (ClientRequestException e) {
            LOGGER.error("Failed to fulfill request", e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    /**
     * Offers the given FisaDocument as a file
     *
     * @param id The ID of the FisaDocument
     *
     * @return The FisaDocument as a file
     *
     */
    @CrossOrigin(origins = "*")
    @GetMapping("/{uuid}/download")
    public ResponseEntity download(
            @PathVariable(value = "uuid")Long id
    ) {
        try {
            FisaDocument document = documentService.getFisaDocument(id);
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.add(
                    "Content-Disposition",
                    "attachment; filename=\"" + document.getName() + ".json\""
            );
            return ResponseEntity.ok()
                    .headers(responseHeaders)
                    .contentType(MediaType.APPLICATION_JSON)
                    .body(document);
        } catch (IOException e) {
            LOGGER.error("Failed to fulfill request", e);
            return ResponseEntity.status(HttpStatus.NOT_FOUND)
                    .body(e.getMessage());
        } catch (ClientRequestException e) {
            LOGGER.error("Failed to fulfill request", e);
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(e.getMessage());
        }
    }
}
