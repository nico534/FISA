package de.fraunhofer.iosb.ilt.fisabackend.service.repository;


import de.fraunhofer.iosb.ilt.fisabackend.model.definitions.FisaProjectSave;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository("fisa_project_repository")
public interface FisaProjectRepository extends JpaRepository<FisaProjectSave, Long> {

    /**
     * Returns the savedProject by name
     * @param name The name of the Project
     * @return the Project if it exists
     */
    @Query("SELECT f FROM fisa_project_save f WHERE f.name = ?1")
    Optional<FisaProjectSave> getProjectSaveByName(String name);
}
