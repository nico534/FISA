package de.fraunhofer.iosb.ilt.fisabackend.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.fraunhofer.iosb.ilt.fisabackend.model.MapNameListElement;
import de.fraunhofer.iosb.ilt.fisabackend.model.definitions.FisaDocument;
import de.fraunhofer.iosb.ilt.fisabackend.model.definitions.FisaDocumentSave;
import de.fraunhofer.iosb.ilt.fisabackend.service.exception.ClientRequestException;
import de.fraunhofer.iosb.ilt.fisabackend.service.repository.FisaDocumentRepository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * The Document service manages all requested operations from the frontend of the application related to the
 * FisaDocument.
 */
public class DocumentService {

    private final FisaDocumentRepository repo;


    /**
     * The document-service Constructor
     *
     * @param repo the FISA Document Repo
     */
    public DocumentService(FisaDocumentRepository repo) {
        this.repo = repo;
    }

    /**
     * Will return a fisa document by it's uuid.
     *
     * @param id the uuid generated from the use-case name
     * @return the fisa document matching the uuid
     * @throws IOException            If DocumentRepository operation failed
     * @throws ClientRequestException if uuid cannot be found in repo
     */
    public FisaDocument getFisaDocument(Long id) throws IOException, ClientRequestException {
        Optional<FisaDocumentSave> item = repo.findById(id);
        if (item.isPresent()) {
            ObjectMapper om = new ObjectMapper();
            return om.readValue(item.get().getData(), FisaDocument.class);
        }
        throw new IOException("Cant't find the given FisaObject with id: " + id);
    }

    /**
     * Will create a List of use-cases currently available in Repository.
     *
     * @return A List of MapNameListElement
     * @throws IOException if DocumentRepository operation failed
     */
    public List<MapNameListElement> listFisaDocument() throws IOException {
        List<FisaDocumentSave> documents = repo.findAll();

        List<MapNameListElement> list = new ArrayList<>();

        for (FisaDocumentSave document : documents) {
            list.add(new MapNameListElement(document.getName(), document.getId()));
        }
        return list;
    }

    /**
     * Saves a FisaDocument in the Document Repository.
     *
     * @param doc The FisaDocument that has to be stored
     * @throws IOException            if DocumentRepository operation failed
     * @throws ClientRequestException if document with same name is already in repository
     */
    public void createFisaDocument(FisaDocument doc) throws IOException, ClientRequestException {
        ObjectMapper objectMapper = new ObjectMapper();
        String data = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(doc);
        FisaDocumentSave toSave = new FisaDocumentSave(doc.getName(), data);
        repo.save(toSave);
    }

    /**
     * removes a FisaDocument from the repository.
     *
     * @param id the id of the FisaDocument that has to be removed
     * @throws IOException            if DocumentRepository operation failed
     * @throws ClientRequestException if no file matching uuid can be found
     */
    public void deleteFisaDocument(Long id) throws IOException, ClientRequestException {
        repo.deleteById(id);
    }
}
