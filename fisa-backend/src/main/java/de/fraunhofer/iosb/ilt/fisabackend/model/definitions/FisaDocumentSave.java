package de.fraunhofer.iosb.ilt.fisabackend.model.definitions;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity(name = "fisa_document_save")
@Table(
        name = "fisa_document_save",
        uniqueConstraints = {
                @UniqueConstraint(name = "document_name_unique", columnNames = "name")
        }
)
public class FisaDocumentSave {
    @Id
    @SequenceGenerator(
            name = "fisa_document_sequence",
            sequenceName = "fisa_document_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "fisa_document_sequence"
    )
    private Long id;

    @Column(
            name = "name",
            nullable = false
    )
    private String name;

    @Column(
            name = "data",
            nullable = false,
            columnDefinition = "TEXT"
    )
    private String data;

    /**
     * Default Constructor
     */
    public FisaDocumentSave() {
    }

    /**
     * Constructor of the FISA document save
     * @param name the name of the project
     * @param data the Json-String representation of the Document
     */
    public FisaDocumentSave(String name, String data) {
        this.name = name;
        this.data = data;
    }

    /**
     * getter for the id
     * @return the id
     */
    public Long getId() {
        return id;
    }

    /**
     * Setter for the id.
     * @param id the new ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * getter for the name.
     * @return the name of the Document
     */
    public String getName() {
        return name;
    }

    /**
     * Setter for the Document-Name
     * @param name the new name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter for the data.
     * @return the JSON-Representation of the saved FISA-Document
     */
    public String getData() {
        return data;
    }

    /**
     * Setter fot the data
     * @param data the new Data.
     */
    public void setData(String data) {
        this.data = data;
    }
}
