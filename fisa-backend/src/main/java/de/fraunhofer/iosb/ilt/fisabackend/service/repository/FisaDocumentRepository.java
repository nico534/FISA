package de.fraunhofer.iosb.ilt.fisabackend.service.repository;

import de.fraunhofer.iosb.ilt.fisabackend.model.definitions.FisaDocumentSave;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository("fisa_document_repository")
public interface FisaDocumentRepository extends JpaRepository<FisaDocumentSave, Long> {
    /**
     * get the Project by name
     * @param name the name to search
     * @return the FisaDocumentSave if it exists
     */
    @Query("SELECT f FROM fisa_document_save f WHERE f.name = ?1")
    Optional<FisaDocumentSave> getProjectSaveByName(String name);
}
