package de.fraunhofer.iosb.ilt.fisabackend.model.definitions;

import java.util.List;

/**
 * The FisaDocument defines the structure of a use-case.
 */
public class FisaDocument {

    private String name;
    private Long id;
    private List<FisaObjectDefinition> objectDefinitions;
    private List<FisaObject> fisaTemplate;

    /**
     * The default constructor of FisaDocument.
     */
    public FisaDocument() {
    }

    /**
     * Instantiates a FisaDocument.
     *
     * @param name Name of the document
     * @param id Identifier
     * @param objectDefinitions Definitions of the use-case
     * @param fisaTemplate Templates for a use-case
     */
    public FisaDocument(
            String name,
            Long id,
            List<FisaObjectDefinition> objectDefinitions,
            List<FisaObject> fisaTemplate
    ) {
        this.name = name;
        this.id = id;
        this.objectDefinitions = objectDefinitions;
        this.fisaTemplate = fisaTemplate;
    }

    /**
     * Returns the name of FisaDocument.
     *
     * @return Name of FisaDocument
     */
    public String getName() {
        return this.name;
    }

    /**
     * Returns the Id of a FisaDocument.
     *
     * @return Id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * Returns the definitions of a use-case.
     *
     * @return Definitions of objects
     */
    public List<FisaObjectDefinition> getObjectDefinitions() {
        return this.objectDefinitions;
    }

    /**
     * Returns templates of a use-case
     *
     * @return Templates of a use-case
     */
    public List<FisaObject> getFisaTemplate() {
        return this.fisaTemplate;
    }

    /**
     * Sets the name of a FisaDocument.
     *
     * @param name Name to be set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Sets the ID of a FisaDocument.
     *
     * @param id ID to be set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Sets the object definitions of a FisaDocument.
     *
     * @param objectDefinitions List of object definitions
     */
    public void setObjectDefinitions(List<FisaObjectDefinition> objectDefinitions) {
        this.objectDefinitions = objectDefinitions;
    }

    /**
     * Sets the templates of a use-case.
     *
     * @param fisaTemplate List of FisaObjects as template
     */
    public void setFisaTemplate(List<FisaObject> fisaTemplate) {
        this.fisaTemplate = fisaTemplate;
    }
}
