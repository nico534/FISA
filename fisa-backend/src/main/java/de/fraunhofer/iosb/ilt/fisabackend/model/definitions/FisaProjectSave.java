package de.fraunhofer.iosb.ilt.fisabackend.model.definitions;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity(name = "fisa_project_save")
@Table(
        name = "fisa_project_save",
        uniqueConstraints = {
                @UniqueConstraint(name = "project_name_unique", columnNames = "name")
        }
)
public class FisaProjectSave {
    @Id
    @SequenceGenerator(
            name = "fisa_project_sequence",
            sequenceName = "fisa_project_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "fisa_project_sequence"
    )
    private Long id;

    @Column(
            name = "name",
            nullable = false
    )
    private String name;

    @Column(
            name = "data",
            nullable = false,
            columnDefinition = "TEXT"
    )
    private String data;

    /**
     * Default Constructor
     */
    public FisaProjectSave() {
    }

    /**
     * Constructor for the FISA-Project save
     * @param name the name of the FISA-Project
     * @param data the Data of the FISA-Project as JSON-Representation
     */
    public FisaProjectSave(String name, String data) {
        this.name = name;
        this.data = data;
    }

    /**
     * Getter for the FISA-Project-save ID
     * @return the FISA-Project-save ID
     */
    public Long getId() {
        return id;
    }

    /**
     * Setter for the FISA-Project-save ID
     * @param id the new save-ID
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Getter for the FISA-Project name
     * @return the name of the FISA-Project
     */
    public String getName() {
        return name;
    }

    /**
     * Setter for the FISA-Project name
     * @param name the new FISA-Project name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter for the FISA-Project data
     * @return the FISA-Project Data
     */
    public String getData() {
        return data;
    }

    /**
     * Setter for the FISA-Project data
     * @param data the new FISA-Project data.
     */
    public void setData(String data) {
        this.data = data;
    }
}
