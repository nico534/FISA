package de.fraunhofer.iosb.ilt.fisabackend.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import de.fraunhofer.iosb.ilt.fisabackend.model.MapNameListElement;
import de.fraunhofer.iosb.ilt.fisabackend.model.definitions.FisaProject;
import de.fraunhofer.iosb.ilt.fisabackend.model.definitions.FisaProjectSave;
import de.fraunhofer.iosb.ilt.fisabackend.service.exception.ClientRequestException;
import de.fraunhofer.iosb.ilt.fisabackend.service.repository.FisaProjectRepository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * The Project service manages all requested operations from the frontend of the application related to the
 * FisaProject.
 */
public class ProjectService {

    private final FisaProjectRepository repo;

    /**
     * Default Constructor
     *
     * @param repo The Fisa-Project repository
     */
    public ProjectService(FisaProjectRepository repo) {
        this.repo = repo;
    }

    /**
     * Will return a fisa project by it's id.
     *
     * @param id the id generated from the project name
     * @return the fisa project matching the id
     * @throws IOException            If ProjectRepository operation failed
     * @throws ClientRequestException if no file matches uuid
     */
    public FisaProject getProject(Long id) throws IOException, ClientRequestException {
        Optional<FisaProjectSave> item = repo.findById(id);
        if (item.isPresent()) {
            ObjectMapper om = new ObjectMapper();
            FisaProject project = om.readValue(item.get().getData(), FisaProject.class);
            // Set the Project-Id to the saved ID
            project.setId(item.get().getId());
            return project;
        }
        throw new IOException("Can't find the given FisaProject with id: " + id);
    }

    /**
     * Will create a List of Projects currently available in Repository.
     *
     * @return key value pair of uuid and name of the FisaProject
     * @throws IOException if ProjectRepository operation failed
     */
    public List<MapNameListElement> listProjects() throws IOException {
        List<FisaProjectSave> projects = repo.findAll();

        List<MapNameListElement> list = new ArrayList<>();

        for (FisaProjectSave project : projects) {
            list.add(new MapNameListElement(project.getName(), project.getId()));
        }
        return list;
    }

    /**
     * Saves a FisaProject in the Project Repository.
     *
     * @param project The FisaProject that has to be stored
     * @throws IOException            if ProjectRepository operation failed
     * @throws ClientRequestException if project name is already occupied
     * @return The ID under which the Project is stored.
     */
    public Long createProject(FisaProject project) throws IOException, ClientRequestException {
        if (checkIfExists(project.getName())) {
            throw new ClientRequestException("A Project with that name already exists");
        }
        ObjectMapper objectMapper = new ObjectMapper();
        String data = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(project);
        FisaProjectSave toSave = new FisaProjectSave(project.getName(), data);
        repo.save(toSave);
        return toSave.getId();
    }

    /**
     * removes a FisaProject from the repository.
     *
     * @param id the id of the FisaProject that has to be removed
     */
    public void deleteProject(Long id) {
        repo.deleteById(id);
    }

    /**
     * updates a FisaProject at the repository.
     *
     * @param project new version of the project to be updated.
     * @throws IOException            if ProjectRepository operation failed
     * @throws ClientRequestException if project could not be found on repository
     */
    public void updateProject(FisaProject project) throws IOException, ClientRequestException {

        Long id = project.getId();
        FisaProject oldVersion = getProject(id);
        if (!oldVersion.getName().equals(project.getName())) {
            throw new ClientRequestException("This is not the same Project-Name");
        }

        ObjectMapper om = new ObjectMapper();

        FisaProjectSave toSave = new FisaProjectSave(
                project.getName(),
                om.writerWithDefaultPrettyPrinter().writeValueAsString(project));

        toSave.setId(project.getId());

        FisaProjectSave savedProject = repo.save(toSave);

        project.setId(savedProject.getId());
    }

    private boolean checkIfExists(String name) {
        return repo.getProjectSaveByName(name).isPresent();
    }
}
